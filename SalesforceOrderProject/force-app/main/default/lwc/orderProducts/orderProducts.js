import { track, LightningElement, wire, api } from 'lwc';
import { subscribe,unsuscribe, MessageContext, publish} from 'lightning/messageService';
import sendProductsToOrder from '@salesforce/messageChannel/SendProductToOrder__c';
import sendActivationOrder from '@salesforce/messageChannel/SendActivationOrder__c';
import getOrderProducts from '@salesforce/apex/OrderProductsCtrl.processSelectedProducts';
import activateOrder from '@salesforce/apex/OrderProductsCtrl.executeOrderActivation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class OrderProducts extends LightningElement {

	@track orderProducts = [];
	susbcription = null;
	@wire(MessageContext)
	messageContext;
	jsonString = '';

	@api recordId;

	connectedCallback(){
		if (!this.susbcription) {
			this.susbcription = subscribe(
				this.messageContext,
				sendProductsToOrder,
				(message) => this.handleSelectedValue(message)
			); 
			
		}
	}

	handleSelectedValue(message){
		
		const selectedProducts = message.filterKey;
		
		getOrderProducts({lstOrderProductsSelected:selectedProducts, lstActualOrderProducts: this.orderProducts})
		.then(result => {
			this.orderProducts = result;
			console.log(this.orderProducts)
		})
		.catch(error => {
			this.error = error;
		});
	}

	activateOrder(){
		const message = {
            active: 'true'
        };

		publish (this.messageContext, sendActivationOrder, message);

		activateOrder({lstOrderProducts: this.orderProducts, recordId: this.recordId})
		.then(result => {
			if (result.success) {
				const evt = new ShowToastEvent({
					title: 'Order Activation',
					message: 'Order Activated succesfully',
					variant: 'success',
				});
				this.dispatchEvent(evt);
			}
			else{
				const evt = new ShowToastEvent({
					title: 'Order Activation Error',
					message: 'Order Activation Error, please contact your administrator',
					variant: 'error',
				});
				this.dispatchEvent(evt);
			}
		})
		.catch(error => {
			this.error = error;
		});
	}

	disconnectedCallback(){
		unsuscribe(this.susbcription);
		this.susbcription = null;
	}

	orderColumns = [
		{
			type: 'text',
			fieldName: 'name',
			label: 'Product Name',
			initialWidth: 300
		},
		{
			type: 'text',
			fieldName: 'unitPrice',
			label: 'Price',
			initialWidth: 300
		},
		{
		   type: 'text',
		   fieldName: 'quantity',
		   label: 'Quantity',
		   initialWidth: 300
	   },
	   {
		type: 'text',
		fieldName: 'totalPrice',
		label: 'Total Price',
		initialWidth: 300
		}
	];

}