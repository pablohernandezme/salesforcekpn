import { LightningElement, wire, track} from 'lwc';
import {
    EXAMPLES_COLUMNS_DEFINITION_BASIC
} from './sampleData';
import getOrdersData from '@salesforce/apex/AvailableProductsCtrl.getOrdersAvailable';
import { subscribe,unsuscribe, MessageContext, publish } from 'lightning/messageService';
import sendProductsToOrder from '@salesforce/messageChannel/SendProductToOrder__c';
import sendActivationOrder from '@salesforce/messageChannel/SendActivationOrder__c';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class AvailableProducts extends LightningElement {

	@wire(MessageContext)
	messageContext;
	@track gridData = [];
	error;
	susbcription = null;
	active = false;
	@track submittedProducts;

	connectedCallback(){
		if (!this.susbcription) {
			this.susbcription = subscribe(
				this.messageContext,
				sendActivationOrder,
				(message) => this.activateOrder(message)
			); 			
		}
	}

	@wire(getOrdersData)
    wiredOrders({ error, data }) {
        if (data) {
			this.gridData = JSON.parse(data);
        } else if (error) {
            this.error = error;
			const evt = new ShowToastEvent({
				title: 'Order Error',
				message: this.error,
				variant: 'error',
			});
			this.dispatchEvent(evt);
        }
    }
	
	gridColumns = EXAMPLES_COLUMNS_DEFINITION_BASIC;

	getSelectedRows(){

		var selected = this.template.querySelector('.lgc-example-treegrid').getSelectedRows();
		this.submittedProducts = selected;

		console.log(this.submittedProducts);
		const payload = { filterKey : this.submittedProducts};

		publish (this.messageContext, sendProductsToOrder, payload);

	}

	activateOrder(message){
		const selectedProducts = message.active;
		if(selectedProducts){
			this.active = true;
		}
	}

	disconnectedCallback(){
		unsuscribe(this.susbcription);
		this.susbcription = null;
	}
}