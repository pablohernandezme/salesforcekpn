 /**
  * Columns definition
  * :: used in examples
  */
  export const EXAMPLES_COLUMNS_DEFINITION_BASIC = [
	{
		type: 'text',
		fieldName: 'Name',
		label: 'Product Name',
		initialWidth: 400
	},
	{
		type: 'text',
		fieldName: 'UnitPrice',
		label: 'Price',
		initialWidth: 400
	}
];