/****************************************************************************************************************
Developed By:        	Juan Pablo Hernández (JH)
Project:                Salesforce Orders by kpn
Description:            HttpCalloutMock class to do a mock of a service
*****************************************************************************************************************/
@isTest
global class HttpCalloutSampleMock implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest req) {

        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setStatusCode(200);
        return res;
    }
}
