/****************************************************************************************************************
Developed By:        	Juan Pablo Hernández (JH)
Project:                Salesforce Orders by kpn
Description:            Controller Class for orderProducts LWC
*****************************************************************************************************************/
public with sharing class OrderProductsCtrl {

	  /*
	  *processSelectedProducts
	  *@param	lstOrderProductsSelected, lstActualOrderProducts list with both selected and actual actual orders.
	  *@return 	List<OrderProduct>
	  *comments 	This method, validates duplicate values in order, add quantity and add total price to the item.
	  */
	  @AuraEnabled(cacheable=true)
	  public static List<OrderProduct> processSelectedProducts(List<SelectedOrderProduct> lstOrderProductsSelected, List<OrderProduct> lstActualOrderProducts ){
		
		List<OrderProduct> orderProducts = new List<OrderProduct>();

		Map<String, SelectedOrderProduct> selectedPrdcMap = createMapFromListSelected(lstOrderProductsSelected);
		Map<String, OrderProduct> actualPrdcMap = createMapFromListActual(lstActualOrderProducts);

		if (selectedPrdcMap.size()>0) {
			for (String eachOrderPrd : selectedPrdcMap.keySet()) {

				if (actualPrdcMap.containsKey(eachOrderPrd)) {
					
					OrderProduct aProduct = new OrderProduct();
					aProduct.name = actualPrdcMap.get(eachOrderPrd).Name;
					aProduct.unitPrice = actualPrdcMap.get(eachOrderPrd).unitPrice;
					aProduct.productCode = actualPrdcMap.get(eachOrderPrd).productCode;
					aProduct.totalPrice = actualPrdcMap.get(eachOrderPrd).totalPrice + actualPrdcMap.get(eachOrderPrd).unitPrice;
					aProduct.quantity = actualPrdcMap.get(eachOrderPrd).quantity +1;
					orderProducts.add(aProduct);
				}
				else {
					OrderProduct aProduct = new OrderProduct();
					aProduct.name = selectedPrdcMap.get(eachOrderPrd).Name;
					aProduct.unitPrice = selectedPrdcMap.get(eachOrderPrd).unitPrice;
					aProduct.productCode = selectedPrdcMap.get(eachOrderPrd).productCode;
					aProduct.totalPrice = selectedPrdcMap.get(eachOrderPrd).unitPrice;
					aProduct.quantity = 1;
					orderProducts.add(aProduct);
				}
			}
		}

		return orderProducts;
	  }

	  /*
	  *createMapFromListSelected
	  *@param		lstObject.
	  *@return  	Map<String, SelectedOrderProduct>
	  *comments 	This method generates a map when the list is received.
	  */
	  public static Map<String, SelectedOrderProduct> createMapFromListSelected(List<SelectedOrderProduct> lstObject){
			
			Map<String, SelectedOrderProduct> objMap = new Map<String, SelectedOrderProduct>();

			for (SelectedOrderProduct eachProduct : lstObject) {
				objMap.put(eachProduct.productCode, eachProduct);
			}

			return objMap;
	  }
	  
	  /*
	  *createMapFromListActual
	  *@param		lstObject.
	  *@return  	Map<String, OrderProduct>
	  *comments 	This method generates a map when the list is received.
	  */
	  public static Map<String, OrderProduct> createMapFromListActual(List<OrderProduct> lstObject){
			
			Map<String, OrderProduct> objMap = new Map<String, OrderProduct>();

			for (OrderProduct eachProduct : lstObject) {
				objMap.put(eachProduct.productCode, eachProduct);
			}

			return objMap;
  		}

		/*
		*executeOrderActivation
		*@param		lstOrderProducts, recordId.
		*@return  	WrpRespuesta
		*comments 	this method generates a callout with the products added to the order.
		*/
		@AuraEnabled(cacheable=true)
		public static WrpRespuesta executeOrderActivation(List<OrderProduct> lstOrderProducts, String recordId){
			  
		try {
			   
			WrpRespuesta wrpRespuesta = new WrpRespuesta();

			Order anOrder = [
				SELECT orderNumber, Contract.ContractNumber,
						  Type, Status
				FROM Order
				WHERE Id =: recordId
				WITH SECURITY_ENFORCED
				LIMIT 1
			  ];

			List<OrderProductsDTO.Request> orderRequest = new List<OrderProductsDTO.Request>();

			OrderProductsDTO.Request orderToServices = new OrderProductsDTO.Request();
			orderToServices.accountNumber = anOrder.Contract.ContractNumber;
			orderToServices.orderNumber = anOrder.OrderNumber;
			orderToServices.type = anOrder.Type;
			orderToServices.status = anOrder.Status;
			orderToServices.orderProducts = new List<OrderProductsDTO.OrderProducts>();

			List<OrderProductsDTO.OrderProducts> lstOrderPrd = new  List<OrderProductsDTO.OrderProducts>();
			
  
			for (OrderProduct eachProcuct : lstOrderProducts) {	
					OrderProductsDTO.orderProducts orderPrd = new OrderProductsDTO.OrderProducts();			  
					orderPrd.code = eachProcuct.productCode;
					orderPrd.name = eachProcuct.name;
					orderPrd.unitPrice = eachProcuct.unitPrice;
					orderPrd.quantity = eachProcuct.quantity;
					lstOrderPrd.add(orderPrd);
			}

			orderToServices.orderProducts = lstOrderPrd;
			orderRequest.add(orderToServices);

			Integer successCode = OrderProductsDTO.serializeData(orderRequest);
			if (successCode == 200) {
				wrpRespuesta.success = true;
			}
			else {
				wrpRespuesta.success = false;
			}
			
			return wrpRespuesta;

		  } catch (Exception Exp) {
				throw new AuraHandledException(Exp.getMessage());
		  }	  
	}

	public class WrpRespuesta{
        @AuraEnabled
        public Boolean success { get; set; }
	}

  	public class SelectedOrderProduct {
		@AuraEnabled public String name { get; set; }
		@AuraEnabled public Integer unitPrice { get; set; }
		@AuraEnabled public String productCode { get; set; }
	}

	public class OrderProduct {
		@AuraEnabled public String name { get; set; }
		@AuraEnabled public Integer unitPrice { get; set; }
		@AuraEnabled public Integer quantity { get; set; }
		@AuraEnabled public Integer totalPrice { get; set; }
		@AuraEnabled public String productCode { get; set; }
	}
}
