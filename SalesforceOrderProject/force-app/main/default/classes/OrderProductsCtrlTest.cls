@IsTest
public with sharing class OrderProductsCtrlTest {

	@IsTest
	static void processSelectedProductsTest(){

		List<OrderProductsCtrl.SelectedOrderProduct> lstSelctProducts =  new List<OrderProductsCtrl.SelectedOrderProduct>();
		
		OrderProductsCtrl.SelectedOrderProduct aSlctProduct1 = new OrderProductsCtrl.SelectedOrderProduct();
		aSlctProduct1.name = 'Selected Product Test 1';
		aSlctProduct1.productCode = 'NG991';
		aSlctProduct1.unitPrice = 9899;

		OrderProductsCtrl.SelectedOrderProduct aSlctProduct2 = new OrderProductsCtrl.SelectedOrderProduct();
		aSlctProduct2.name = 'Selected Product Test 2';
		aSlctProduct2.productCode = 'NG992';
		aSlctProduct2.unitPrice = 1056;

		lstSelctProducts.add(aSlctProduct1);
		lstSelctProducts.add(aSlctProduct2);


		List<OrderProductsCtrl.OrderProduct> lstOrderProduct = new List<OrderProductsCtrl.OrderProduct>();

		OrderProductsCtrl.OrderProduct aOrderProduct1 = new OrderProductsCtrl.OrderProduct();
		aOrderProduct1.name = 'Actual product Order 1';
		aOrderProduct1.productCode = 'NG991';
		aOrderProduct1.quantity = 1;
		aOrderProduct1.totalPrice = 958;
		aOrderProduct1.unitPrice = 9899;

		OrderProductsCtrl.OrderProduct aOrderProduct2 = new OrderProductsCtrl.OrderProduct();
		aOrderProduct2.name = 'Actual product Order 2';
		aOrderProduct2.productCode = 'NG993';
		aOrderProduct2.quantity = 1;
		aOrderProduct2.totalPrice = 695;
		aOrderProduct2.unitPrice = 258;

		lstOrderProduct.add(aOrderProduct1);
		lstOrderProduct.add(aOrderProduct2);

		Test.startTest();
		
		List<OrderProductsCtrl.OrderProduct> lstOrderProducts = OrderProductsCtrl.processSelectedProducts(lstSelctProducts, lstOrderProduct);

		System.assertEquals(lstOrderProducts[0].quantity, 2, 'Error to obtain quantity of the lstOrderProducts');
		System.assertEquals(lstOrderProducts[1].quantity, 1, 'Error to obtain quantity of the lstOrderProducts');

		Test.stopTest();
		
	}

	@IsTest
	static void executeOrderActivationTest(){

		List<OrderProductsCtrl.OrderProduct> lstOrderProduct = new List<OrderProductsCtrl.OrderProduct>();

		OrderProductsCtrl.OrderProduct aOrderProduct1 = new OrderProductsCtrl.OrderProduct();
		aOrderProduct1.name = 'Actual product Order 1';
		aOrderProduct1.productCode = 'NG991';
		aOrderProduct1.quantity = 1;
		aOrderProduct1.totalPrice = 958;
		aOrderProduct1.unitPrice = 9899;

		OrderProductsCtrl.OrderProduct aOrderProduct2 = new OrderProductsCtrl.OrderProduct();
		aOrderProduct2.name = 'Actual product Order 2';
		aOrderProduct2.productCode = 'NG993';
		aOrderProduct2.quantity = 1;
		aOrderProduct2.totalPrice = 695;
		aOrderProduct2.unitPrice = 258;

		lstOrderProduct.add(aOrderProduct1);
		lstOrderProduct.add(aOrderProduct2);

		Account anAccount = new Account();
		anAccount.Name = 'Test Acc';
		
		insert anAccount;

		Contract aContract = new Contract();
		aContract.Status = 'Draft';
		aContract.Name = 'Example Contrat';
		aContract.AccountId = anAccount.Id;

		insert aContract;

		Order anOrder = new Order();
		anOrder.type = 'delivery';
		anOrder.Status = 'draft';
		anOrder.ContractId = aContract.Id;
		anOrder.AccountId = anAccount.Id;
		anOrder.EffectiveDate = Date.today();
		
		insert anOrder;

		Test.setMock(HttpCalloutMock.class, new HttpCalloutSampleMock());
		
		Test.startTest();
		OrderProductsCtrl.WrpRespuesta wrRespTest = OrderProductsCtrl.executeOrderActivation(lstOrderProduct, anOrder.Id);
		System.assertEquals(true, wrRespTest.success, 'Error to retrieve statuscode from callout');
		Test.stopTest();
	}
    
}
