/****************************************************************************************************************
Developed By:        	Juan Pablo Hernández (JH)
Project:                Salesforce Orders by kpn
Description:            Test Class for Apex Controller AvailableProductsCtrl
*****************************************************************************************************************/
@IsTest
public with sharing class AvailableProductsCtrlTest {
    
	@TestSetup
	static void makeData(){

		Product2 prod = new Product2();
		prod.Name = 'Laptop X200';
		prod.Family = 'Hardware';
		prod.ProductCode = 'NHGPO';
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();

		PriceBookEntry aPricebookEntry = new PriceBookEntry();
		aPricebookEntry.Pricebook2Id = pricebookId;
		aPricebookEntry.Product2Id = prod.Id;
		aPricebookEntry.UnitPrice = 1287;
		aPricebookEntry.IsActive = true;

		insert aPricebookEntry;
		
	}


	@IsTest
	static void getOrdersAvailableTest(){
		
		String orderProductsJson = AvailableProductsCtrl.getOrdersAvailable();

		Test.startTest();
		System.assertEquals(true, orderProductsJson.contains('Laptop X200'), 'Error to obtain Products Name');
		System.assertEquals(true, orderProductsJson.contains('NHGPO'), 'Error to obtain Products Code');
		System.assertEquals(false, orderProductsJson.contains('XXXX'), 'Error to obtain Products That no exist');
		Test.stopTest();
		
	}

}
