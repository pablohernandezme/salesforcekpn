/****************************************************************************************************************
Developed By:        	Juan Pablo Hernández (JH)
Project:                Salesforce Orders by kpn
Description:            HttpCallout class to do a simple request service
*****************************************************************************************************************/
public with sharing class HttpCalloutSample {
	
	// Pass in the endpoint to be used using the string url and body
	public HttpResponse getCalloutResponseContents(String url, String body, String method) {
	
	  // Instantiate a new http object
	  Http h = new Http();
	
	   // Instantiate a new HTTP request, specify the method as well as the endpoint
	  HttpRequest req = new HttpRequest();
	  req.setEndpoint('callout:'+url+'/test');
	  req.setBody(body);
	  req.setMethod(method);
	
	  // Send the request, and return a response
	  HttpResponse res = h.send(req);
	  return res;
	}

}
