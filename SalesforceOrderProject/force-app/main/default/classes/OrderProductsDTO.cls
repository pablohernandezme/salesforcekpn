/****************************************************************************************************************
Developed By:        	Juan Pablo Hernández (JH)
Project:                Salesforce Orders by kpn
Description:            DT Class for executeOrderService
*****************************************************************************************************************/
public with sharing class OrderProductsDTO {

    public class Request{
		@AuraEnabled
        public String accountNumber { get; set; }
        @AuraEnabled
        public String orderNumber { get; set; }
        @AuraEnabled
        public String type { get; set; }
        @AuraEnabled
        public String status { get; set; }
        @AuraEnabled
        public List<OrderProducts> orderProducts { get; set; }
	}

	public class OrderProducts{
		@AuraEnabled
		public String name { get; set; }
        @AuraEnabled
        public String code { get; set; }
        @AuraEnabled
        public Integer unitPrice { get; set; }
        @AuraEnabled
        public Integer quantity { get; set; }
	}
	
	/*
	*serializeData
	*@param		bodyRequest.
	*@return  	Integer statusCode of callout operation
	*comments 	This method generates a map when the list is received.
	*/
    public static Integer serializeData (List<request> bodyRequest)
	{
        String jsonRequest =  JSON.serialize(bodyRequest);

		HttpCalloutSample req = new HttpCalloutSample();
		HttpResponse response = req.getCalloutResponseContents('salesforcekpn', jsonRequest, 'POST');

        return response.getStatusCode();
    }
}
