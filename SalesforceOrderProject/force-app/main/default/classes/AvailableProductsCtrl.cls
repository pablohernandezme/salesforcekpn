/****************************************************************************************************************
Developed By:        	Juan Pablo Hernández (JH)
Project:                Salesforce Orders by kpn
Description:            Controller Class for LWC availableProducts
*****************************************************************************************************************/
public with sharing class AvailableProductsCtrl {

	
	/**
     * getOrdersAvailable method to get all orders available and active.
     * @return String with jsonData for tree grid of the controller.
     */
	@AuraEnabled(Cacheable=true)
	public static String getOrdersAvailable(){

		List<PriceBookEntry> lstPriceBookEntry = [
			SELECT Id, Name, UnitPrice, ProductCode, IsActive
			FROM PriceBookEntry
			WHERE IsActive = true
			WITH SECURITY_ENFORCED
		];

		Map<String,PriceBookEntry> duplicateCtrlProductMap = new Map<String,PriceBookEntry>();

		for (PriceBookEntry eachPriceBookEntry : lstPriceBookEntry) {
			
			if (!duplicateCtrlProductMap.containsKey(eachPriceBookEntry.ProductCode)) {
				duplicateCtrlProductMap.put(eachPriceBookEntry.ProductCode, eachPriceBookEntry);
			}
 
		}

		JSONGenerator gen = JSON.createGenerator(true);
		
		gen.writeStartArray();
		
		for (String eachPriceBookEntry : duplicateCtrlProductMap.keySet()) {
			gen.writeStartObject();
			gen.writeStringField('Name', duplicateCtrlProductMap.get(eachPriceBookEntry).Name);
			gen.writeNumberField('UnitPrice', duplicateCtrlProductMap.get(eachPriceBookEntry).UnitPrice);
			gen.writeStringField('ProductCode', duplicateCtrlProductMap.get(eachPriceBookEntry).ProductCode);
			gen.writeEndObject();
		}
		gen.writeEndArray();       
		
        String pretty = gen.getAsString();

		return pretty;
	}
}
